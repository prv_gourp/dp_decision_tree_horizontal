import pykka

from pandas import DataFrame
from main import Message, AddSecuredData, GetClassifiersCount, Ack


class MyActor(pykka.ThreadingActor):
    # MyActor will have the logic of a single party which calculates its sum of each attribute
    # and aggregate the result to the last actor which then returns the decrypted result to main
    # who will calculate the max information gain and call ID3 recursively
    # each actor will hold its table, and next actor ref. (they all doing the same calc)

    def __init__(self, data_frame: DataFrame, index: int):
        super().__init__()
        self.data_frame = data_frame
        self.index = index

    def on_start(self):
        ...  # My optional setup code in same context as on_receive()

    def on_stop(self):
        ...  # My optional cleanup code in same context as on_receive()

    def on_failure(self, exception_type, exception_value, traceback):
        ...  # My optional cleanup code in same context as on_receive()

    def on_receive(self, message: Message):
        # My optional message handling code for a plain actor
        # on receiving message, calculate through homomorphic library the Num of each field
        # of the attribute (maybe save it through a dic)
        if type(message) is AddSecuredData:
            return self.handle_add_secured_data(message)
        if type(message) is GetClassifiersCount:
            return self.handle_get_classifiers_count(message)

    def handle_add_secured_data(self, add_secured_data: AddSecuredData):

        # Iterating the relevant attributes
        for attr in add_secured_data.attributes:

            # ..
            if attr not in add_secured_data.encrypted_data_schema:
                add_secured_data.encrypted_data_schema[attr] = {}

            # Generating the count of each classifier given attribute
            groups = self.data_frame.query(add_secured_data.query).groupby([attr, add_secured_data.target_variable]).size()

            # Iterating the classifier's counters given the attribute values
            for i, (attr_value, classifier) in enumerate(groups.index):

                # Verify the attribute value is not in data schema
                if attr_value not in add_secured_data.encrypted_data_schema[attr]:

                    # Adding a dictionary of {attr_value : { classifier: value } }
                    add_secured_data.encrypted_data_schema[attr][attr_value] = \
                        {classifier: add_secured_data.encryption.encrypt(groups.values[i])}

                # ..
                elif classifier not in add_secured_data.encrypted_data_schema[attr][attr_value]:
                    add_secured_data.encrypted_data_schema[attr][attr_value].update({
                        classifier: add_secured_data.encryption.encrypt(groups.values[i])
                    })

                # ..
                else:
                    add_secured_data.encrypted_data_schema[attr][attr_value][classifier] = \
                        add_secured_data.encrypted_data_schema[attr][attr_value][classifier] + \
                        add_secured_data.encryption.encrypt(groups.values[i])

        if len(add_secured_data.send_to) - 1 == self.index:
            return add_secured_data

        return add_secured_data.send_to[self.index + 1].ask(add_secured_data)

    def handle_get_classifiers_count(self, classifiers_count: GetClassifiersCount):
        cur_classifiers_count = self.data_frame[classifiers_count.target_variable].value_counts()
        classifiers_count_dict = dict(zip(cur_classifiers_count.index, cur_classifiers_count.values))

        for key in classifiers_count_dict:
            if key not in classifiers_count.encrypted_data_schema[classifiers_count.target_variable]:
                classifiers_count.encrypted_data_schema[classifiers_count.target_variable][key] = \
                    classifiers_count.encryption.encrypt(classifiers_count_dict[key])
            else:
                classifiers_count.encrypted_data_schema[classifiers_count.target_variable].update({
                    key: classifiers_count.encrypted_data_schema[classifiers_count.target_variable][key] +
                    classifiers_count.encryption.encrypt(classifiers_count_dict[key])})

        if len(classifiers_count.send_to) - 1 == self.index:
            return classifiers_count

        return classifiers_count.send_to[self.index + 1].ask(classifiers_count)
