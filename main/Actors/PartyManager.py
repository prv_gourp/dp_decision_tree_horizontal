from functools import reduce

import numpy
import pykka

from main import Message, GetClassifiersCount, GenerateDecisionTree, Encryption, AddSecuredData, Node, Tree


class PartyManager(pykka.ThreadingActor):
    # MyActor will have the logic of a single party which calculates its sum of each attribute
    # and aggregate the result to the last actor which then returns the decrypted result to main
    # who will calculate the max information gain and call ID3 recursively
    # each actor will hold its table, and next actor ref. (they all doing the same calc)

    def __init__(self):
        super().__init__()

    def on_start(self):
        ...  # My optional setup code in same context as on_receive()

    def on_stop(self):
        ...  # My optional cleanup code in same context as on_receive()

    def on_failure(self, exception_type, exception_value, traceback):
        ...  # My optional cleanup code in same context as on_receive()

    def on_receive(self, message: Message):
        # My optional message handling code for a plain actor
        # on receiving message, calculate through homomorphic library the Num of each field
        # of the attribute (maybe save it through a dic)
        if type(message) is GenerateDecisionTree:
            return self.handle_generate_decision_tree(message)

    @staticmethod
    def decrypt_classifiers_count(data: dict, target_variable: str, encryption: Encryption):
        """
        Decrypts GetClassifiersCount message
        :param data: The data schema {target_variable: {c1: v1, c2: v2, ..., cn: vn}}
        :param target_variable: The data target variable
        :param encryption: The encryption method
        :return: Decrypts the data in place
        """
        for classifier in data[target_variable]:
            data[target_variable][classifier] = encryption.decrypt(data[target_variable][classifier])

    @staticmethod
    def decrypt_secured_data(data: dict, encryption: Encryption):
        """
        Decrypts AddSecuredData message
        :param data: The data schema {attr1: {attr1_v1: {c1: v1, c2: v2, ..., cn: vn}, attr1_v2: {...}}, attr2: {...}}
        :param encryption: The encryption method
        :return: Decrypts the data in place
        """
        for attribute in data:
            for attribute_value in data[attribute]:
                for classifier in data[attribute][attribute_value]:
                    data[attribute][attribute_value][classifier] = \
                        encryption.decrypt(data[attribute][attribute_value][classifier])

    @staticmethod
    def calculate_entropy(classifiers_values):
        """
        Calculates and returns the entropy
        :param classifiers_values: A list of classifiers values
        :return: The calculated entropy based on the formula below
        """
        total = sum(classifiers_values)

        return reduce(lambda acc, x: acc + (-1) * ((x / total) * numpy.log2(x / total)), classifiers_values, 0)

    @staticmethod
    def get_parties_classifiers_count(target_variable, parties, encryption):
        """
        Sends GetClassifiersCount message between the parties in a cycle manner and returns classifiers,
         entropy and total_classifiers_values
        :param target_variable: The data target variable
        :param parties: The parties where their data is horizontally partitioned
        :param encryption: The encryption method
        :return: classifiers, entropy, total_classifiers_values
        """
        classifiers_count: GetClassifiersCount = GetClassifiersCount({target_variable: {}}, target_variable,
                                                                     parties,
                                                                     encryption)

        # Sends the GetClassifiersCount message between the parties and waits for response
        data: GetClassifiersCount = parties[0].ask(classifiers_count, block=True)

        # Decrypts the data received
        PartyManager.decrypt_classifiers_count(data.encrypted_data_schema, target_variable, encryption)
        classifiers_values = list(data.encrypted_data_schema[target_variable].values())

        return data.encrypted_data_schema[target_variable], PartyManager.calculate_entropy(classifiers_values), \
               sum(classifiers_values)

    @staticmethod
    def get_secured_data(target_variable, parties, encryption, attributes, query):
        """
        Sends AddSecuredData message between the parties in a cycle manner and returns the decrypted data
        :param target_variable: The data target variable
        :param parties: The parties where their data is horizontally partitioned
        :param encryption: The encryption method
        :param attributes: The current left attributes
        :param query: A predicate represents the current branch
        :return: The decrypted data
        """
        secured_data: AddSecuredData = AddSecuredData({}, attributes, target_variable,
                                                      parties, encryption, query)

        # Sends the AddSecuredData message between the parties and waits for response
        data: AddSecuredData = parties[0].ask(secured_data, block=True)

        # Decrypts and returns the data received
        PartyManager.decrypt_secured_data(data.encrypted_data_schema, encryption)
        return data.encrypted_data_schema

    @staticmethod
    def get_most_frequent_classifier(transactions, target_variable):
        """
        Calculates and returns the most frequent classifier in the given transactions
        :param transactions: The data set
        :param target_variable: The data target variable
        :return: The most frequent classifier in the given transactions
        """
        classifier_values_to_classifier = {transactions[target_variable][classifier][classifier]: classifier
                                           for classifier in transactions[target_variable]}
        return classifier_values_to_classifier[max(classifier_values_to_classifier)]

    @staticmethod
    def is_same_classifiers(transactions, target_variable):
        """
        Returns True iff all the transactions have the same classifier
        :param transactions: The data set
        :param target_variable: The data target variable
        :return: True iff all the transactions have the same classifier
        """
        classifiers = list(filter(lambda element: element != 0, [transactions[target_variable][classifier][classifier]
                                                                 for classifier in transactions[target_variable]]))
        return len(classifiers) == 1

    @staticmethod
    def get_best_attribute(transactions, total_classifiers_values, entropy, target_variable):
        """
        Calculates and returns the best attribute that classifies the data (with the highest information gain)
        :param transactions: The data set
        :param total_classifiers_values: Sum of classifiers values
        :param entropy: The total entropy that was calculated at the beginning
        :param target_variable: The data target variable
        :return: The best attribute that classifies the data (with the highest information gain)
        """
        max_information_gain = -1
        best_attr = ""

        # Iterating the data attributes, for each attribute calculating the entropy given the attribute by summing
        # the weighted average of an attribute values entropyies.
        for attr in transactions:
            entropy_given_attribute = 0

            for attr_value in transactions[attr]:
                branched_classifier_values = sum(list(transactions[attr][attr_value].values()))
                entropy_given_attribute += (branched_classifier_values / total_classifiers_values) * \
                                           PartyManager.calculate_entropy(transactions[attr][attr_value].values())

            if max_information_gain < entropy - entropy_given_attribute and attr != target_variable:
                max_information_gain = entropy - entropy_given_attribute
                best_attr = attr

        return best_attr

    @staticmethod
    def is_empty_transactions(transactions: dict):
        """
        Returns True iff the transactions has empty data
        :param transactions: The data set
        :return: True iff the transactions has empty data
        """
        return all([not v for (k, v) in transactions.items()])

    @staticmethod
    def id3(init_data, attributes, classifiers, transactions, target_variable, total_classifiers_values, entropy,
            parties, encryption, query: str, parent_most_frequent_classifier):
        if PartyManager.is_empty_transactions(transactions):
            return Node(parent_most_frequent_classifier)
        elif not attributes or ((len(attributes) == 1) and target_variable in attributes):
            return Node(PartyManager.get_most_frequent_classifier(transactions, target_variable))
        elif PartyManager.is_same_classifiers(transactions, target_variable):
            return Node(PartyManager.get_most_frequent_classifier(transactions, target_variable))
        else:
            best_attr = PartyManager.get_best_attribute(transactions, total_classifiers_values, entropy,
                                                        target_variable)
            best_attr_values = init_data[best_attr]
            new_attributes = [attr for attr in attributes if attr != best_attr]
            parent_most_frequent_classifier = PartyManager.get_most_frequent_classifier(transactions, target_variable)

            if not query:
                query_n = best_attr + "=="
            else:
                query_n = query + ' and ' + best_attr + "=="

            branched_transactions = [(PartyManager.get_secured_data(target_variable, parties, encryption,
                                                                    new_attributes,
                                                                    query_n + "\"" + str(attr_value) + '\"'),
                                      attr_value)
                                     for attr_value in best_attr_values]

            children = {attr_value: PartyManager.id3(init_data, new_attributes, classifiers,
                                                     branch, target_variable, total_classifiers_values,
                                                     entropy, parties, encryption,
                                                     query_n + "\"" + str(attr_value) + '\"',
                                                     parent_most_frequent_classifier)
                        for (branch, attr_value) in branched_transactions}

            return Node(best_attr, children)

    def handle_generate_decision_tree(self, generate_decision_tree: GenerateDecisionTree):

        classifiers, entropy, total_classifiers_values = PartyManager.get_parties_classifiers_count(
            generate_decision_tree.target_variable,
            generate_decision_tree.parties,
            generate_decision_tree.encryption)

        data = PartyManager.get_secured_data(generate_decision_tree.target_variable,
                                             generate_decision_tree.parties,
                                             generate_decision_tree.encryption,
                                             generate_decision_tree.attributes,
                                             'tuple()')

        decision_tree: Tree = Tree(PartyManager.id3(data, generate_decision_tree.attributes, classifiers, data,
                                                    generate_decision_tree.target_variable,
                                                    total_classifiers_values,
                                                    entropy, generate_decision_tree.parties,
                                                    generate_decision_tree.encryption, '',
                                                    PartyManager.get_most_frequent_classifier(data,
                                                                                              generate_decision_tree.target_variable)))

        return decision_tree
