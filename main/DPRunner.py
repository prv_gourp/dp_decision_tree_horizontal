import argparse

from main import MyActor, PartyManager, EncryptionFactory, GenerateDecisionTree, Tree, DataStatistics,\
    DataProcessor, DataProcessorFactory


def main(data_path, number_of_samples, number_of_parties, target_variable, encryption_method,
         supplied_test_set, test_data_path, use_training_set, percentage_split, percentage_split_value):
    data_processor: DataProcessor = DataProcessorFactory.get_data_processor(data_path, number_of_samples,
                                                                            number_of_parties)
    data_processor.process()
    samples_bucket = data_processor.sample(supplied_test_set, test_data_path, use_training_set,
                                           percentage_split, percentage_split_value)
    actors = [MyActor.start(samples_bucket[i], i) for i in range(number_of_parties)]

    party_manager = PartyManager.start()

    decision_tree: Tree = party_manager.ask(GenerateDecisionTree(actors, target_variable,
                                            EncryptionFactory.get_encryption_method(encryption_method),
                                            list(samples_bucket[0].columns)))

    for i in range(len(actors)):
        actors[i].stop()

    party_manager.stop()

    return decision_tree, DataStatistics.get_stats(decision_tree, samples_bucket[-1], target_variable)


def parse_program_arguments():
    parser = argparse.ArgumentParser()

    parser.add_argument("-d", "--data_path", help="The data path in csv format", type=str,
                        default="../kaggle/input/income_evaluation.csv")
    parser.add_argument("-s", "--number_of_samples", help="The number of samples to collect", type=int, default=110)
    parser.add_argument("-p", "--number_of_parties", help="The number of parties to split the samples among",
                        type=int, default=10)
    parser.add_argument("-t", "--target_variable", help="The target variable name", type=str, default="income")
    parser.add_argument("-e", "--encryption_method", help="The encryption method", type=str, default="homomorphic",
                        choices=["homomorphic"])
    parser.add_argument("-sts", "--supplied_test_set", help="Test on user-specified dataset", type=bool, default=False)
    parser.add_argument("-tdp", "--test_data_path", help="Test on user-specified dataset", type=str, default="")
    parser.add_argument("-uts", "--use_training_set", help="Test on the same set that the classifier is trained on",
                        type=bool, default=False)
    parser.add_argument("-ps", "--percentage_split", help="Test on the percentage of data and train on the reminder",
                        type=bool, default=True)
    parser.add_argument("-psv", "--percentage_split_value", help="Test on the percentage of data and train on"
                                                                 " the reminder", type=int, default=10)

    return parser.parse_args()


if __name__ == '__main__':
    args = parse_program_arguments()
    decision_tree_res, stats = main(args.data_path, args.number_of_samples, args.number_of_parties,
                                    args.target_variable, args.encryption_method, args.supplied_test_set,
                                    args.test_data_path, args.use_training_set, args.percentage_split,
                                    args.percentage_split_value)
    print(stats)
