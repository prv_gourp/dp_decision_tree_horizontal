from abc import ABC, abstractmethod

import numpy
import pandas


class DataProcessor(ABC):

    def __init__(self, data, num_of_samples, num_of_parties):
        super().__init__()
        self.data = data
        self.df = pandas.read_csv(data)
        self.num_of_samples = num_of_samples
        self.num_of_parties = num_of_parties

    @staticmethod
    def get_meta_data(data_path):
        """
        Gets a data path in csv format and returns the attributes, number of rows
        :param data_path: data path in csv format
        :return: Attributes and number of rows
        """
        df = pandas.read_csv(data_path)
        return list(df.columns.values), len(df.index)

    @abstractmethod
    def process(self):
        pass

    def sample(self, supplied_test_set, test_data_path, use_training_set, percentage_split, percentage_split_value):
        samples = self.df.sample(n=self.num_of_samples)
        buckets = []

        if supplied_test_set:
            buckets = numpy.split(samples, self.num_of_parties)
            buckets.append(pandas.read_csv(test_data_path))
        elif use_training_set:
            buckets = numpy.split(samples, self.num_of_parties)
            buckets.append(samples)
        elif percentage_split:
            sample_parties = [(i + 1) * (int(self.num_of_samples * (1 - (percentage_split_value / 100)) /
                              self.num_of_parties)) for i in range(self.num_of_parties)]
            buckets = numpy.split(samples, sample_parties)

        return buckets
