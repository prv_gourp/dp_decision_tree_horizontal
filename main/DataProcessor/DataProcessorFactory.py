import ntpath

from main import IncomeEvaluationDataProcessor, USAccidentsDataProcessor, DefaultDataProcessor


class DataProcessorFactory:
    def __init__(self):
        super().__init__()

    @staticmethod
    def get_data_processor(data, num_of_samples, num_of_parties):
        if ntpath.basename(data) == "income_evaluation.csv":
            return IncomeEvaluationDataProcessor(data, num_of_samples, num_of_parties)
        elif ntpath.basename(data) == "us_accidents.csv":
            return USAccidentsDataProcessor(data, num_of_samples, num_of_parties)
        else:
            return DefaultDataProcessor(data, num_of_samples, num_of_parties)
