import numpy

from main import DataProcessor


class DefaultDataProcessor(DataProcessor):
    def __init__(self, data, num_of_samples, num_of_parties):
        super().__init__(data, num_of_samples, num_of_parties)

    def process(self):
        pass
