import pandas
import numpy

from main import DataProcessor


class IncomeEvaluationDataProcessor(DataProcessor):
    def __init__(self, data, num_of_samples, num_of_parties):
        """
        :param data: data path in csv format
        :param num_of_samples: number of samples to collect
        :param num_of_parties: number of parties to split the samples among
        """
        super().__init__(data, num_of_samples, num_of_parties)

    def process(self):
        """
        :return: num_of_parties + 1 sub arrays contains samples from the data
        """
        col_names = ['age', 'workclass', 'fnlwgt', 'education', 'education_num', 'marital_status', 'occupation',
                     'relationship', 'race', 'sex', 'capital_gain', 'capital_loss', 'hours_per_week',
                     'native_country', 'income']
        irrelevant_column = ['fnlwgt', 'relationship', 'capital_gain', 'capital_loss']

        # Renaming the columns
        self.df.rename(columns=dict(zip([' ' + t.replace('_', '-') for t in col_names], col_names)), inplace=True)

        # Dropping irrelevant columns
        self.df.drop(columns=irrelevant_column, inplace=True)

        # Dropping rows with unknown values
        self.df.replace({' ?': numpy.NaN}, inplace=True)
        self.df.dropna(inplace=True)

        # Group age attributes data into discrete bins
        age_labels = ["{0} - {1}".format(i, i + 9) for i in range(0, 100, 10)]
        self.df['age'] = pandas.cut(self.df['age'], range(0, 105, 10), right=False, labels=age_labels)

        # Group hours_per_week attributes data into discrete bins
        hours_per_week_labels = ["{0} - {1}".format(i, i + 4) for i in range(0, 100, 5)]
        self.df['hours_per_week'] = pandas.cut(self.df['hours_per_week'], range(0, 105, 5), right=False,
                                               labels=hours_per_week_labels)
