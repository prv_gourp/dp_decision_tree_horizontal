import numpy
import pandas

from main import DataProcessor


class USAccidentsDataProcessor(DataProcessor):
    def __init__(self, data, num_of_samples, num_of_parties):
        super().__init__(data, num_of_samples, num_of_parties)

    def process(self):
        irrelevant_column = ['ID', 'Source', 'TMC', 'Start_Time', 'End_Time', 'Start_Lat', 'Start_Lng', 'End_Lat',
                             'End_Lng', 'Distance', 'Description', 'Number', 'Street', 'Side', 'City', 'County',
                             'State', 'Zipcode', 'Country', 'Timezone', 'Airport_Code', 'Weather_Timestamp',
                             'Pressure', 'Wind_Direction', 'Precipitation']

        # Dropping irrelevant columns
        self.df.drop(columns=irrelevant_column, inplace=True)

        # Dropping rows with unknown values
        self.df.replace({' ?': numpy.NaN}, inplace=True)
        self.df.dropna(inplace=True)

        # Group Temperature attributes data into discrete bins
        temp_labels = ["{0} - {1}".format(i, i + 19) for i in range(0, 240, 20)]
        self.df['Temperature'] = pandas.cut(self.df['Temperature'], range(0, 260, 20), right=False, labels=temp_labels)

        # Group Wind_Chill attributes data into discrete bins
        wind_chill_labels = ["{0} - {1}".format(i, i + 19) for i in range(0, 240, 20)]
        self.df['Wind_Chill'] = pandas.cut(self.df['Wind_Chill'], range(0, 260, 20), right=False,
                                           labels=wind_chill_labels)

        # Group Humidity attributes data into discrete bins
        humidity_labels = ["{0} - {1}".format(i, i + 9) for i in range(0, 110, 10)]
        self.df['Humidity'] = pandas.cut(self.df['Humidity'], range(0, 120, 10), right=False, labels=humidity_labels)

        # Group Visibility attributes data into discrete bins
        visibility_labels = ["{0} - {1}".format(i, i + 1) for i in range(0, 20, 2)]
        self.df['Visibility'] = pandas.cut(self.df['Visibility'], range(0, 22, 2), right=False,
                                           labels=visibility_labels)

        # Group Wind_Speed attributes data into discrete bins
        wind_speed_labels = ["{0} - {1}".format(i, i + 4) for i in range(0, 40, 5)]
        self.df['Wind_Speed'] = pandas.cut(self.df['Wind_Speed'], range(0, 45, 5), right=False,
                                           labels=wind_speed_labels)
