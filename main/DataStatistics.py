from pandas import DataFrame

from main import Tree, Node


class DataStatistics:
    @staticmethod
    def get_prediction(node: Node, data):
        if not node.children:
            return node.data
        return DataStatistics.get_prediction(node.children[data[node.data]], data)

    @staticmethod
    def get_stats(decision_tree: Tree, test_data_set: DataFrame, target_variable: str):
        correctly_classified_instances = 0
        incorrectly_classified_instances = 0
        train_and_test_set_incompatibility = 0

        for index, row in test_data_set.iterrows():
            try:
                if DataStatistics.get_prediction(decision_tree.root, row) == row[target_variable]:
                    correctly_classified_instances += 1
                else:
                    incorrectly_classified_instances += 1

            except KeyError:
                train_and_test_set_incompatibility += 1

        return('Privacy Preserving Decision Tree Over Horizontally Partitioned Data\n'
               '-------------------------------------------------------------------\n' +
               str(decision_tree) +
               '\n\n=== Summary ==='
               '\n\tCorrectly Classified Instances\t\t%d\t\t%f%%'
               '\n\tIncorrectly Classified Instances\t\t%d\t\t%f%%'
               '\n\tTrain and test set incompatibility\t\t%d\t\t%f%%' %
               (correctly_classified_instances, correctly_classified_instances * 100 / test_data_set.shape[0],
                incorrectly_classified_instances, incorrectly_classified_instances * 100 / test_data_set.shape[0],
                train_and_test_set_incompatibility, train_and_test_set_incompatibility * 100 / test_data_set.shape[0]))
