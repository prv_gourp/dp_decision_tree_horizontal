from abc import ABC, abstractmethod


class Encryption(ABC):

    def __init__(self):
        super().__init__()

    @abstractmethod
    def encrypt(self, value):
        pass

    @abstractmethod
    def decrypt(self, encrypted_value):
        pass
