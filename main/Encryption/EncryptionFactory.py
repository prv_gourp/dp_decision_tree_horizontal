from main import HomomorphicEncryption


class EncryptionFactory():
    def __init__(self):
        super().__init__()

    @staticmethod
    def get_encryption_method(encryption_method):
        if encryption_method == "homomorphic":
            return HomomorphicEncryption()
