from Pyfhel import Pyfhel
from main import Encryption


class HomomorphicEncryption(Encryption):
    def __init__(self):
        super().__init__()
        self.he = Pyfhel()
        self.he.contextGen(65537)
        self.he.keyGen()

    def encrypt(self, value):
        return self.he.encrypt(value)

    def decrypt(self, encrypted_value):
        return self.he.decrypt(encrypted_value, True)
