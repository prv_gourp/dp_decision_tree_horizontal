class Node:

    def __init__(self, data, children=None):
        if children is None:
            self.children = {}
        else:
            self.children = children
        self.data = data

    def add_child(self, child):
        self.children.append(child)

    def __str__(self, level=0):
        ret = "\t" * level + repr(self.data) + "\n"
        for child in self.children:
            ret += "\t" * (level + 1) + str(child) + "\n" + self.children[child].__str__(level + 2)
        return ret

    def __repr__(self):
        return '<tree node representation>'
