from collections import deque
from random import random

from graphviz import Digraph

from main import Node


class Tree:
    def __init__(self, root: Node):
        self.root = root

    def __str__(self):
        return self.root.__str__()

    def visual_tree(self):
        dot = Digraph(comment='Privacy Preserving Decision Tree Over Horizontally Partitioned Data')

        # BFS traversing of the tree and generating the graph
        if self.root:
            node_queue = deque()

            count = 0
            dot.node(str(count), self.root.data)
            node_queue.append((str(count), self.root))

            while len(node_queue) > 0:
                (parent_name, parent_node) = node_queue.popleft()

                if parent_node.children:
                    for child in parent_node.children:
                        count += 1
                        dot.node(str(count), str(parent_node.children[child].data))
                        dot.edge(parent_name, str(count), str(child))

                        node_queue.append((str(count), parent_node.children[child]))
        return dot
