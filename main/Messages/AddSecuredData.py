from main import Message, Encryption


class AddSecuredData(Message):
    def __init__(self, encrypted_data_schema, attributes, target_variable, send_to, encryption: Encryption, query):
        super().__init__()
        self.encrypted_data_schema = encrypted_data_schema
        self.attributes = attributes
        self.target_variable = target_variable
        self.send_to = send_to
        self.encryption = encryption
        self.query = query
