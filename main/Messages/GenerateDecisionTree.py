from main import Message, Encryption


class GenerateDecisionTree(Message):
    def __init__(self, parties: [], target_variable, encryption: Encryption, attributes):
        super().__init__()
        self.parties = parties
        self.target_variable = target_variable
        self.encryption = encryption
        self.attributes = attributes
