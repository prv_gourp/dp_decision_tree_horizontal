from pykka import ActorRef

from main import Message, Encryption


class GetClassifiersCount(Message):
    def __init__(self, encrypted_data_schema, target_variable, send_to, encryption: Encryption):
        super().__init__()
        self.encrypted_data_schema = encrypted_data_schema
        self.target_variable = target_variable
        self.send_to = send_to
        self.encryption = encryption
