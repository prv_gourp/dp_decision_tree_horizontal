#!/usr/bin/env python

from distutils.core import setup

setup(name='DPDecisionTreeHorizontal',
      version='1.0',
      description='Data Preserving Decision Tree Horizontal',
      packages=['main', 'main.Actors', 'main.Messages', 'main.Encryption'],
      install_requires=['pykka', 'pandas', 'numpy', 'Pyfhel', 'pysimplegui', 'graphviz']
      )
