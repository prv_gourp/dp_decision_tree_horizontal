import pykka

from main import Message
from test import GetMessages


class MockActor(pykka.ThreadingActor):
    # A mocking actor that returns every message it gets
    def __init__(self):
        super().__init__()
        self.received_messages = []

    def on_receive(self, message: Message):
        if type(message) is GetMessages:
            return self.received_messages
        else:
            self.received_messages.append(message)
            return None
