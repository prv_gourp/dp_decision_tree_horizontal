import unittest

from main import HomomorphicEncryption


class HomomorphicEncryptionTest(unittest.TestCase):

    def test_add_cipher_text(self):
        he = HomomorphicEncryption()
        c1 = he.encrypt(3)
        c2 = he.encrypt(7)
        self.assertEqual(10, he.decrypt(c1 + c2))

    def test_sub_cipher_text(self):
        he = HomomorphicEncryption()
        c1 = he.encrypt(100)
        c2 = he.encrypt(29)
        self.assertEqual(71, he.decrypt(c1 - c2))

    def test_mul_cipher_text(self):
        he = HomomorphicEncryption()
        c1 = he.encrypt(4)
        c2 = he.encrypt(12)
        self.assertEqual(48, he.decrypt(c1 * c2))
