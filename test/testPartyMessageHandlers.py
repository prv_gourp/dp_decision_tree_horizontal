import json
import unittest

import pandas
from pandas import DataFrame
from pykka import ActorRef

from main import GetClassifiersCount, MyActor, HomomorphicEncryption, AddSecuredData, Message
from test import MockActor, GetMessages


class PartyMessageHandlersTest(unittest.TestCase):

    def setUp(self):
        self.setData("./input/sample_processed_data_simple.csv")
        self.encryption: HomomorphicEncryption = HomomorphicEncryption()
        self.mock: ActorRef = MockActor.start()
        self.subject: ActorRef = MyActor.start(self.df, 0)

    def tearDown(self):
        self.mock.stop()
        self.subject.stop()

    def setData(self, csv):
        self.df: DataFrame = pandas.read_csv(csv)
        self.target_variable = "income"
        self.classifiers = self.df[self.target_variable].value_counts()
        self.attributes = list(self.df.columns)

    def decrypt_classifiers_counters(self, cc):
        for k in cc.encrypted_data_schema[cc.target_variable]:
            cc.encrypted_data_schema[cc.target_variable].update({
                k: cc.encryption.decrypt(cc.encrypted_data_schema[cc.target_variable][k])
            })

    def decrypt_secured_data(self, sc):
        for attribute in sc.encrypted_data_schema:
            for attribute_value in sc.encrypted_data_schema[attribute]:
                for classifier in sc.encrypted_data_schema[attribute][attribute_value]:
                    sc.encrypted_data_schema[attribute][attribute_value][classifier] = \
                        sc.encryption.decrypt(sc.encrypted_data_schema[attribute][attribute_value][classifier])

    def ask_and_decrypt(self, message: Message, decrypt_func):
        self.subject.ask(message)
        cc = self.mock.ask(GetMessages(), timeout=5)[0]
        decrypt_func(cc)

        return cc.encrypted_data_schema

    def test_handle_get_classifiers_count_exists_data(self):
        initial_value = 100

        # Generating a starting point of encrypted data schema with initial values
        classifiers_count_dict = {self.target_variable: dict(zip(self.classifiers.index,
                                                                 [self.encryption.encrypt(initial_value)] *
                                                                 len(self.classifiers.values)))}

        # Generating the expected output dictionary
        classifiers_expected_count_dict = {self.target_variable: dict(zip(self.classifiers.index,
                                                                          [(t + initial_value)
                                                                           for t in self.classifiers.values]))}

        classifiers_count_message = GetClassifiersCount(classifiers_count_dict, self.target_variable,
                                                        [None, self.mock], self.encryption)

        self.assertEqual(classifiers_expected_count_dict, self.ask_and_decrypt(classifiers_count_message,
                                                                               self.decrypt_classifiers_counters))

    def test_handle_get_classifiers_count_empty_data(self):

        # Generating the expected output dictionary
        classifiers_expected_count_dict = {self.target_variable: dict(zip(self.classifiers.index,
                                                                          self.classifiers.values))}

        classifiers_count_message = GetClassifiersCount({self.target_variable: {}},
                                                        self.target_variable, [None, self.mock], self.encryption)

        self.assertEqual(classifiers_expected_count_dict, self.ask_and_decrypt(classifiers_count_message,
                                                                               self.decrypt_classifiers_counters))

    def test_handle_add_secured_data_empty_data_branched(self):

        #  Random attribute value that will act as a predicate (current branch)
        attr_value = self.df[self.attributes[0]][0]

        # Parsing the expected output dictionary
        with open('./output/sample_processed_data_simple_age_branch.json') as json_file:
            expected_output_dict = json.load(json_file)

        query = self.attributes[0] + "==" + "\"" + str(attr_value) + "\""

        add_secured_data_message = AddSecuredData({}, self.attributes[1:],
                                                  self.target_variable, [None, self.mock], self.encryption, query)

        self.assertEqual(expected_output_dict, self.ask_and_decrypt(add_secured_data_message,
                                                                    self.decrypt_secured_data))

    def test_handle_add_secured_data(self):

        #  Random attribute value that will act as a predicate (current branch)
        attr_k = self.attributes[0]
        attr_v = self.df[attr_k][0]

        query = attr_k + "==" + "\"" + str(attr_v) + "\""

        # Parsing the expected output dictionary
        with open('./output/sample_processed_data_simple_age_branch_expected.json') as json_file:
            expected_output_dict = json.load(json_file)

        # Parsing the given dictionary
        with open('./output/sample_processed_data_simple_2_age_branch.json') as json_file:
            given_data = json.load(json_file)

        # Encrypt given data
        for attr in given_data:
            for attr_value in given_data[attr]:
                for classifier in given_data[attr][attr_value]:
                    given_data[attr][attr_value][classifier] = \
                        self.encryption.encrypt(given_data[attr][attr_value][classifier])

        add_secured_data_message = AddSecuredData(given_data, self.attributes[1:],
                                                  self.target_variable, [None, self.mock], self.encryption, query)

        self.assertEqual(expected_output_dict, self.ask_and_decrypt(add_secured_data_message,
                                                                    self.decrypt_secured_data))
